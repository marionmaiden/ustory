package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mario
 */
@Entity
@Table(name = "PROJECT")
@XmlRootElement
public class Project implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NAME")
    private String name;
    @Size(max = 140)
    @Column(name = "DESCRIPTION")
    private String description;
    @OneToMany(mappedBy = "project", fetch = FetchType.EAGER, targetEntity = Theme.class, cascade = CascadeType.ALL)
    private List<Theme> themes;
    @OneToMany(mappedBy = "project", targetEntity = Story.class, cascade = CascadeType.ALL)
    private List<Story> stories;
    
    public Project() {
        themes = new ArrayList<>();
        stories = new ArrayList<>();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Project)) {
            return false;
        }
        Project other = (Project) object;
        return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
    }

    @Override
    public String toString() {
        return "model.Project[ id=" + id + " ]";
    }

    /**
     * @return the themes
     */
    public List<Theme> getThemes() {
        return themes;
    }

    /**
     * @param themes the themes to set
     */
    public void setThemes(List<Theme> themes) {
        this.themes = themes;
    }

    /**
     * @return the stories
     */
    public List<Story> getStories() {
        return stories;
    }

    /**
     * @param stories the stories to set
     */
    public void setStories(List<Story> stories) {
        this.stories = stories;
    }

}
