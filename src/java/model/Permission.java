package model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
 
@Entity(name = "PERMISSION")
public class Permission implements Serializable {
 
    @Id
    @Column(name = "PERMISSION")
    private String permission;
 
    public Permission() {
    }
 
    //getter and setter

    /**
     * @return the permission
     */
    public String getPermission() {
        return permission;
    }

    /**
     * @param permission the permission to set
     */
    public void setPermission(String permission) {
        this.permission = permission;
    }


    
}