package Util;

import javax.faces.context.FacesContext;
import javax.faces.el.ValueBinding;

/**
 * Controls the session of the page
 *
 * @author mario
 */
public class SessionUtil {

    /**
     * Sets a session variable some value
     * @param facesVarName the managedBean name. Eg. "#{storyBean.projects}"
     * @param newValue 
     */
    public static void setVariableValue(String facesVarName, Object newValue) {
        FacesContext ctx = FacesContext.getCurrentInstance();
        ValueBinding bind = ctx.getApplication().createValueBinding(facesVarName);

        Class bindClass = bind.getType(ctx);
        if (!bindClass.isPrimitive()) {
            if (newValue == null || bindClass.isInstance(newValue)) {
                bind.setValue(ctx, newValue);
            }
        } else {
            bind.setValue(ctx, newValue);
        }
    }
    
    
}
