package Util;

/**
 *
 * @author mario
 */
public enum StatusEnum {
    todo("bg-danger"),
    doing("bg-info"),
    done("bg-success");
    
    private final String statusClass;

    private StatusEnum(String statusClass) {
        this.statusClass = statusClass;
    }

    /**
     * @return the statusClass
     */
    public String getStatusClass() {
        return statusClass;
    }
    
}
