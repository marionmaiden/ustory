
import model.Permission;
import model.User;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.tool.hbm2ddl.SchemaExport;

/**
 * Create Database Tables
 * @author mario
 */
public class CreateDB {
    public static void main(String args[]){
        try {
        AnnotationConfiguration ac = new AnnotationConfiguration();
        ac.addAnnotatedClass(User.class);
        ac.addAnnotatedClass(Permission.class);        
        SessionFactory sessionFactory = ac.configure().buildSessionFactory();
        SchemaExport se = new SchemaExport(ac);
        se.create(true, true);
    }catch (Throwable ex) {
        System.err.println("Initial SessionFactory creation failed." + ex);
        throw new ExceptionInInitializerError(ex);
     }
    }
}
