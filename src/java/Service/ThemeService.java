package Service;

import Util.HibernateUtil;
import java.util.List;
import model.Theme;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author mariomhgf
 */
public class ThemeService implements GenericService{
    
    /**
     * Gets the list of Themes
     * @return 
     */
    @Override
    public List<? extends Object> getList() {
        Session session = HibernateUtil.getSession();
        List<Object> themes = session.createCriteria(Theme.class).list();
        session.close();
        return themes;
    }

    /**
     * Gets the list of Themes not linked to any project
     * @return 
     */
    public List<? extends Object> getListOrphanTheme() {
        Session session = HibernateUtil.getSession();
        List<Object> themes = session.createCriteria(Theme.class)
                .add(Restrictions.eq("PROJECT_ID", 0)).list();
        session.close();
        return themes;
    }
    
    
    /**
     * Searches a Theme by it's ID
     * @param id
     * @return 
     */
    @Override
    public Object getById(Integer id) {
        Session session = HibernateUtil.getSession();
        List<Object> themes = session.createCriteria(Theme.class).add(Restrictions.eq("id", id)).list();
        session.close();
        
        if(themes.size() > 0){
            return themes.get(0);
        }
        return null;
    }

    /**
     * Inserts a new Theme on DB
     * @param o 
     */
    @Override
    public void insert(Object o) {
        Theme t = (Theme)o;
        Session session = HibernateUtil.getSession();
        Transaction trx = session.beginTransaction();
        session.merge(t);
        trx.commit();
        session.close();
    }

    /**
     * Updates a Theme on DB
     * @param o 
     */
    @Override
    public void update(Object o) {
        Theme t = (Theme)o;
        Session session = HibernateUtil.getSession();
        Transaction trx = session.beginTransaction();
        session.update(t);
        trx.commit();
        session.close();
    }

    /**
     * Deletes a Theme on DB
     * @param o 
     */
    @Override
    public void delete(Object o) {
        Theme t = (Theme)o;
        Session session = HibernateUtil.getSession();
        Transaction trx = session.beginTransaction();
        session.delete(t);
        trx.commit();
        session.close();
    }       
}

