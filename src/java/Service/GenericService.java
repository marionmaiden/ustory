package Service;

import java.util.List;

/**
 * Interface que define os métodos obrigatórios em um Service:
 * Listar
 * Obter objeto pelo ID
 * Inserir
 * Atualizar
 * Remover
 * @author mariomhgf
 */
public interface GenericService {
        
    List<? extends Object> getList();
    
    Object getById (Integer id);
    
    void insert(Object o);
    
    void update(Object o);
    
    void delete(Object o);
    
}
