package Service;

import Util.HibernateUtil;
import java.util.List;
import model.Story;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author mariomhgf
 */
public class StoryService implements GenericService{

    /**
     * Gets the list of User Stories
     * @return 
     */
    @Override
    public List<? extends Object> getList() {
        Session session = HibernateUtil.getSession();
        List<Object> stories = session.createCriteria(Story.class).list();
        session.close();
        return stories;
    }

    /**
     * Searches an User Story by it's ID
     * @param id
     * @return 
     */
    @Override
    public Object getById(Integer id) {
        Session session = HibernateUtil.getSession();
        List<Object> stories = session.createCriteria(Story.class).add(Restrictions.eq("id", id)).list();
        session.close();
        
        if(stories.size() > 0){
            return stories.get(0);
        }
        return null;
    }

    public List<? extends Object> getByProjectId(Integer id) {
        Session session = HibernateUtil.getSession();
        List<Object> stories = session.createCriteria(Story.class).add(Restrictions.eq("project.id", id)).list();
        session.close();

        return stories;
    }
    
    
    /**
     * Inserts a new User Story on DB
     * @param o 
     */
    @Override
    public void insert(Object o) {
        Story s = (Story)o;
        Session session = HibernateUtil.getSession();
        Transaction trx = session.beginTransaction();
        session.merge(s);
        trx.commit();
        session.close();
    }

    /**
     * Updates a User Story on DB
     * @param o 
     */
    @Override
    public void update(Object o) {
        Story s = (Story)o;
        Session session = HibernateUtil.getSession();
        Transaction trx = session.beginTransaction();
        session.update(s);
        trx.commit();
        session.close();
    }

    /**
     * Deletes a User Story on DB
     * @param o 
     */
    @Override
    public void delete(Object o) {
        Story s = (Story) o;
        Session session = HibernateUtil.getSession();
        Transaction trx = session.beginTransaction();
        session.delete(s);
        trx.commit();
        session.close();
    }    
}
