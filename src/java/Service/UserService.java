package Service;

import Util.HibernateUtil;
import java.util.List;
import model.Project;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author mariomhgf
 */
public class UserService implements GenericService{
    
    /**
     * Gets the list of Projects
     * @return 
     */
    @Override
    public List<? extends Object> getList() {
        Session session = HibernateUtil.getSession();
        List<Object> projects = session.createCriteria(Project.class).list();       
        session.close();
        return projects;
    }

    /**
     * Searches a Project by it's ID
     * @param id
     * @return 
     */
    @Override
    public Object getById(Integer id) {
        Session session = HibernateUtil.getSession();
        Object project = session.createCriteria(Project.class)
                .add(Restrictions.eq("id", id)).uniqueResult();
        session.close();
        
        return project;
    }

    /**
     * Inserts a new Project on DB
     * @param o 
     */
    @Override
    public void insert(Object o) {
        Project p = (Project)o;
        Session session = HibernateUtil.getSession();
        Transaction trx = session.beginTransaction();
        session.merge(p);
        trx.commit();
        session.close();
    }

    /**
     * Updates a Project on DB
     * @param o 
     */
    @Override
    public void update(Object o) {
        Project p = (Project)o;
        Session session = HibernateUtil.getSession();
        Transaction trx = session.beginTransaction();
        session.update(p);
        trx.commit();
        session.close();
    }

    /**
     * Deletes a Project on DB
     * @param o 
     */
    @Override
    public void delete(Object o) {
        Project p = (Project)o;
        Session session = HibernateUtil.getSession();
        Transaction trx = session.beginTransaction();
        session.delete(p);
        trx.commit();
        session.close();
    }       
}
