package Converter;

import Service.GenericService;
import Service.ProjectService;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.ConverterException;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import model.Project;

/**
 *
 * @author mariomhgf
 */
@ManagedBean
@FacesConverter(forClass = Project.class, value = "projectConverter")
public class ProjectConverter implements Converter {

    private GenericService service = new ProjectService();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.isEmpty()) {
            return null;
        }

        if (!value.matches("[0-9]+")) {
            throw new ConverterException("The value is not a valid Project ID: " + value);
        }

        Integer id = Integer.valueOf(value);
        return service.getById(id);
    }

    @Override    
    public String getAsString(FacesContext context, UIComponent component, Object value) {        
        if (value == null) {
            return "";
        }

        if (!(value instanceof Project)) {
            throw new ConverterException("The value is not a valid Student instance: " + value);
        }

        Integer id = ((Project)value).getId();
        return (id != null) ? String.valueOf(id) : null;
   } 
}
