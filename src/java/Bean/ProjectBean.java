package Bean;

import Service.GenericService;
import Service.ProjectService;
import Service.ThemeService;
import Util.SessionUtil;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import model.Project;
import model.Theme;

/**
 *
 * @author mariomhgf
 */
@ManagedBean
@ViewScoped
public class ProjectBean implements Serializable {

    private GenericService serviceP;
    private GenericService serviceT;
    private Project project;
    private Theme editedTheme;

    @PostConstruct
    public void init() {
        serviceP = serviceP == null ? new ProjectService() : serviceP;
        serviceT = serviceT == null ? new ThemeService() : serviceT;

        editedTheme = new Theme();

        if (getProject() == null) {
            setProject(new Project());
        }
    }

    /**
     * @return the project
     */
    public Project getProject() {
        return project;
    }

    /**
     * @param project the project to set
     */
    public void setProject(Project project) {
        this.project = project;
    }

    /**
     * @return the editedTheme
     */
    public Theme getEditedTheme() {
        return editedTheme;
    }

    public void resetTheme() {
        this.editedTheme = new Theme();
    }

    /**
     * @param editedTheme the editedTheme to set
     */
    public void setEditedTheme(Theme editedTheme) {
        this.editedTheme = editedTheme;
        this.editedTheme = new Theme();
    }

    public void saveTheme() {
        this.editedTheme.setProject(project);
        this.editedTheme = new Theme();
    }

    public String saveProject() {
        // Inclusion
        if (project.getId() != null) {
            serviceP.update(project);
        } // Edition
        else {
            serviceP.insert(project);
        }

        this.project = new Project();

        // This guy renews the session, stablishing the list of projects
        SessionUtil.setVariableValue("#{storyBean.projects}", serviceP.getList());

        return "projects";
    }

}
