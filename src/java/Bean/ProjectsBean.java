package Bean;

import Service.GenericService;
import Service.ProjectService;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import model.Project;

/**
 *
 * @author mariomhgf
 */
@ManagedBean
@ViewScoped
public class ProjectsBean implements Serializable{
    
    private List<Project> projects;
    private Project editedProject;
    private GenericService service;
    
    @PostConstruct
    public void init(){
        service = service == null ? new ProjectService() : service;        
        setEditedProject(new Project());
        setProjects((List<Project>) service.getList());
        
    }

    public void delete(){
        service.delete(editedProject);
        // the redefinition of new editedProject is done on init()
        this.init();
        // TODO include the facesmessage informing the exclusion
    }
    
    /**
     * @return the projects
     */
    public List<Project> getProjects() {
        return projects;
    }

    /**
     * @param projects the projects to set
     */
    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    /**
     * @return the editedProject
     */
    public Project getEditedProject() {
        return editedProject;
    }

    /**
     * @param editedProject the editedProject to set
     */
    public void setEditedProject(Project editedProject) {
        this.editedProject = editedProject;
    }
}
