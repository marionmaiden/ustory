package Bean;

import Service.GenericService;
import Service.ProjectService;
import Service.StoryService;
import Util.StatusEnum;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import model.Project;
import model.Story;

/**
 *
 * @author mariomhgf
 */
@ManagedBean
@SessionScoped
public class StoryBean implements Serializable {

    // Coordinates and user story id for mooving purposes
    private Integer x;
    private Integer y;
    private Integer id;

    // Auxiliary objects
    private Story editStory;
    private Project selectedProject;

    // Service objects
    private StoryService serviceS;
    private GenericService serviceP;

    // Lists
    // The Story list is loaded just when a project is selected
    private List<Project> projects;
    private List<Story> stories;

    @PostConstruct
    public void init() {
        // Initialize services
        serviceS = serviceS == null ? new StoryService() : serviceS;
        serviceP = serviceP == null ? new ProjectService() : serviceP;

        // Initialize auxiliary objects
        editStory = new Story();
        this.setSelectedProject(selectedProject == null ? new Project() : selectedProject);

        // temporarily added
        //stories = (List<Story>) serviceS.getList();
        
        // Initialize projects array
        projects = (List<Project>) serviceP.getList();
    }

    /**
     * *
     * When a project is set in the dropdown menu, the stories of that project
     * are loaded
     */
     public boolean isWithoutProject() {
         return selectedProject == null;
     }
     
    
    /**
     * Update the position of an user story based on it's coordinates
     */
    public void updatePosition() {
        // boolean flag to inform that some user story has been found
        boolean found = false;
        
        // If some element was moved
        if (id != null) {
            // Loops al the user stories
            for (int i = 0; i < getStories().size() && !found; i++) {
                // If the user story moved was found
                if (Objects.equals(getStories().get(i).getId(), id)) {
                    // Update the element coordinates
                    getStories().get(i).setX(x);
                    getStories().get(i).setY(y);
                    // Records the updated element
                    serviceS.update(getStories().get(i));
                    found = true;
                }
            }
        }
    }

    /**
     * Saves some user story
     */
    public void saveStory() {
        // If the user Story doesn't have coordinates
        if (editStory.getX() == null || editStory.getY() == null) {
            editStory.setX(0);
            editStory.setY(0);
        }
        // Set the project wich this user story belong
        if (selectedProject != null) {
            editStory.setProject(selectedProject);
        }
        editStory.setStatusEnum(StatusEnum.todo);
        
        serviceS.insert(editStory);

        init();
    }

    private void updateStatus(StatusEnum st){
        // boolean flag to inform that some user story has been found
        boolean found = false;
        
        if (id != null) {
            // Loops al the user stories
            for (int i = 0; i < getStories().size() && !found; i++) {
                // If the user story moved was found
                if (Objects.equals(getStories().get(i).getId(), id)) {
                    getStories().get(i).setStatusEnum(st);
                    serviceS.update(getStories().get(i));
                    found = true;
                }
            }
        }
    }
    
    public void updateTodo(){
        updateStatus(StatusEnum.todo);
    }

    public void updateDoing(){
        updateStatus(StatusEnum.doing);
    }
    
    
    public void updateDone(){
        updateStatus(StatusEnum.done);
    }    
    
    public void deleteStory(){
        // boolean flag to inform that some user story has been found
        boolean found = false;
        
        if (id != null) {
            // Loops al the user stories
            for (int i = 0; i < getStories().size() && !found; i++) {
                // If the user story moved was found
                if (Objects.equals(getStories().get(i).getId(), id)) {
                    serviceS.delete(getStories().get(i));
                    setSelectedProject(this.selectedProject);
                    found = true;
                }
            }
        }
    }        
    
    /**
     * @return the stories
     */
    public List<Story> getStories() {
        return stories;
    }

    /**
     * @param stories the stories to set
     */
    public void setStories(List<Story> stories) {
        this.stories = stories;
    }

    /**
     * @return the x
     */
    public Integer getX() {
        return x;
    }

    /**
     * @param x the x to set
     */
    public void setX(Integer x) {
        this.x = x;
    }

    /**
     * @return the y
     */
    public Integer getY() {
        return y;
    }

    /**
     * @param y the y to set
     */
    public void setY(Integer y) {
        this.y = y;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the editStory
     */
    public Story getEditStory() {
        return editStory;
    }

    /**
     * @param editStory the editStory to set
     */
    public void setEditStory(Story editStory) {
        this.editStory = editStory;
    }

    /**
     * @return the projects
     */
    public List<Project> getProjects() {
        return projects;
    }

    /**
     * @param projects the projects to set
     */
    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    /**
     * @return the selectedProject
     */
    public Project getSelectedProject() {
        return selectedProject;
    }

    /**
     * Return the name of the selected project. If no project is selected
     * the method returns a message to select the project
     * @return 
     */
    public String getProjectName(){
        if(selectedProject == null || selectedProject.getId() == null){
            return "Select project";
        }
        else{
            return selectedProject.getName();
        }
    }
    
    /**
     * @param selectedProject the selectedProject to set
     */
    public void setSelectedProject(Project selectedProject) {
        this.selectedProject = selectedProject;
        if (selectedProject != null) {
            stories = (List<Story>) serviceS.getByProjectId(selectedProject.getId());
        }
        else if(selectedProject == null || selectedProject.getId() == 0){
            stories.clear();
        }
    }
}
